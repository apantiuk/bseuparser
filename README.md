# BSEU Parser

Parses Belarus State Economic University's staff pages from
http://bseu.by/PersonalPages/alphabetic.htm

## Requirements

- Python 3
- Beautiful Soup 4 (`bs4`)
- `requests`


## How to use

To start script execute the current package like following

```sh
python bseuParser
```

and pass necessary options.

---

To get help execute the script like `python . --help`.
You will see the following:

```
usage: . [-h] [-f FILE_PATH] [-q] [-v] [--pretty]

Parses Belarus State Economic University\'s staff list.

optional arguments:
  -h, --help            show this help message and exit
  -f FILE_PATH, --file FILE_PATH
                        output results to passed file path
  -q, --quiet           supress all logs
  -v, --verbose         output all values, has an effect if -q or --quiet is
                        not set
  --pretty              pretty output to console and/or files
```

### Examples

### Run script to store each timetable in separate JSON file

```sh
python . -imf data --pretty
```

All results are stored in passed directory (in the example `data`).

Options:

- `--pretty` allows you to have readable JSON files _(optional)_
- `--multi` improves performance by writing all file right after parsing

### Run script to store each timetable in a single JSON file

```sh
python . -f bseu.json --pretty
```

All results are stored in passed file (in the example `bseu.json`).
The file is an array of parsed timetables.
